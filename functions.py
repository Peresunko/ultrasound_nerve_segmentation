from os import listdir, mkdir, rmdir
from os.path import isfile, join, exists, splitext
import sys
import random
import numpy as np
import time
from shutil import copyfile, rmtree
import cv2

import matplotlib.pyplot as plt

from itertools import chain
from skimage.io import imread, imshow, imread_collection, concatenate_images, imsave
from skimage.transform import resize
from skimage.morphology import label

import tensorflow as tf
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import Lambda
from tensorflow.keras.layers import Conv2D, Conv2DTranspose, BatchNormalization, Dropout
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import concatenate
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, TensorBoard
from tensorflow.keras import backend as K, metrics
K.set_image_data_format('channels_last')


def save_img_dataset(img_path, new_height, new_width, verbose=False):
    """Loads images from img_path and saves it to .npy file with new size (new_height, new_width). 
    It allows us to load dataset faster."""
    samples = [f[:-4] for f in listdir(img_path) if isfile(join(img_path, f)) and not f.endswith('_mask.tif')]
    X = []
    Y = []
    i = 0
    for i, sample in enumerate(samples):
        img = imread(img_path + sample + '.tif', cv2.IMREAD_GRAYSCALE)
        img = resize(img, (new_height, new_width, 1), mode='constant', preserve_range=True)
        X.append(img)

        mask = imread(img_path + sample + '_mask.tif')
        mask = resize(mask, (new_height, new_width, 1), mode='constant', preserve_range=True)
        Y.append(mask)
        if verbose and i % 100 == 0:
            print('Loaded {} samples from {}'.format(i, len(samples)))
    
    X = np.array(X)
    Y = np.array(Y)
    np.save('X_train-{}-{}.npy'.format(new_height, new_width), X)
    np.save('Y_train-{}-{}.npy'.format(new_height, new_width), Y)



def preprocess_data(X_train, Y_train=None, mean=None, std=None):
    """Normalizes all images and gets mean and std of dataset."""
    X_train = X_train.astype('float32')
    if mean is None:
        mean = np.mean(X_train)
        std = np.std(X_train)

    X_train -= mean
    X_train /= std
    if Y_train is not None:
        Y_train = Y_train.astype('float32')
        Y_train /= 255.
    return X_train, Y_train, mean, std
	
	
def dice_coef(y_true, y_pred):
    numerator = 2 * tf.reduce_sum(y_true * y_pred, axis=-1)
    denominator = tf.reduce_sum(y_true + y_pred, axis=-1)
    return (numerator + 1) / (denominator + 1)

	
def add_unet_left_block(filters, conv_kernel_size=(3, 3), pool_kernel_size=(2, 2), 
                   add_normalization=False, add_dropout=False, dropout_prob=0.1):
    """We can consider UNet as principle of building ANN with different amount of building blocks.
    This function creates descending (left) blocks of UNet."""

    def f(s):
        c = Conv2D(filters, conv_kernel_size, activation='relu', padding='same') (s)
        if add_normalization:
            c = BatchNormalization() (c)
        c = Conv2D(filters, conv_kernel_size, activation='relu', padding='same') (c)
        if add_normalization:
            c = BatchNormalization() (c)
        p = MaxPooling2D(pool_kernel_size) (c)
        if add_dropout:
            p = Dropout(dropout_prob)(p)
        return p, c
    return f


def add_unet_right_block(filters, conv_kernel_size=(3, 3), add_normalization=False):
    """This function creates ascending (right) blocks of UNet."""
    def f(c11, c22):
        u = Conv2DTranspose(filters, (2, 2), strides=(2, 2), padding='same') (c11)
        u = concatenate([u, c22])
        c = Conv2D(filters, conv_kernel_size, activation='relu', padding='same') (u)
        if add_normalization:
            c = BatchNormalization() (c)
        c = Conv2D(filters, conv_kernel_size, activation='relu', padding='same') (c)
        if add_normalization:
            c = BatchNormalization() (c)
        return c
    return f


def getStandartUnet(IMG_HEIGHT, IMG_WIDTH, add_normalization=False, add_dropout=False, dropout_prob=0.1,
                   filters=[8, 16, 32, 64, 128]):
    """Build UNet with 4 levels."""
    inputs = Input((IMG_HEIGHT, IMG_WIDTH, 1))

    p1, c1 = add_unet_left_block(filters[0], add_normalization=add_normalization, 
                                 add_dropout=add_dropout, dropout_prob=dropout_prob) (inputs)
    p2, c2 = add_unet_left_block(filters[1], add_normalization=add_normalization, 
                                 add_dropout=add_dropout, dropout_prob=dropout_prob) (p1)
    p3, c3 = add_unet_left_block(filters[2], add_normalization=add_normalization, 
                                 add_dropout=add_dropout, dropout_prob=dropout_prob) (p2)
    p4, c4 = add_unet_left_block(filters[3], add_normalization=add_normalization, 
                                 add_dropout=add_dropout, dropout_prob=dropout_prob) (p3)

    c5 = Conv2D(filters[4], (3, 3), activation='relu', padding='same') (p4)
    c5 = Conv2D(filters[4], (3, 3), activation='relu', padding='same') (c5)

    c6 = add_unet_right_block(filters[3], add_normalization=add_normalization)(c5, c4)
    c7 = add_unet_right_block(filters[2], add_normalization=add_normalization)(c6, c3)
    c8 = add_unet_right_block(filters[1], add_normalization=add_normalization)(c7, c2)
    c9 = add_unet_right_block(filters[0], add_normalization=add_normalization)(c8, c1)
    
    outputs = Conv2D(1, (1, 1), activation='sigmoid') (c9)

    model = Model(inputs=[inputs], outputs=[outputs])
    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=[dice_coef])
    return model


def getUnet3levels(IMG_HEIGHT, IMG_WIDTH, add_normalization=False, add_dropout=False, dropout_prob=0.1,
                   filters=[8, 16, 32, 64]):
    """Build UNet with 3 levels."""
    inputs = Input((IMG_HEIGHT, IMG_WIDTH, 1))

    p1, c1 = add_unet_left_block(filters[0], add_normalization=add_normalization, 
                                 add_dropout=add_dropout, dropout_prob=dropout_prob) (inputs)
    p2, c2 = add_unet_left_block(filters[1], add_normalization=add_normalization, 
                                 add_dropout=add_dropout, dropout_prob=dropout_prob) (p1)
    p3, c3 = add_unet_left_block(filters[2], add_normalization=add_normalization, 
                                 add_dropout=add_dropout, dropout_prob=dropout_prob) (p2)

    c4 = Conv2D(filters[3], (3, 3), activation='relu', padding='same') (p3)
    c4 = Conv2D(filters[3], (3, 3), activation='relu', padding='same') (c4)

    c5 = add_unet_right_block(filters[2], add_normalization=add_normalization)(c4, c3)
    c6 = add_unet_right_block(filters[1], add_normalization=add_normalization)(c5, c2)
    c7 = add_unet_right_block(filters[0], add_normalization=add_normalization)(c6, c1)
    
    outputs = Conv2D(1, (1, 1), activation='sigmoid') (c7)

    model = Model(inputs=[inputs], outputs=[outputs])
    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=[dice_coef])
    return model



def getUnet5levels(IMG_HEIGHT, IMG_WIDTH, add_normalization=False, add_dropout=False, dropout_prob=0.1,
                   filters=[8, 16, 32, 64, 128, 256]):
    """Build UNet with 5 levels."""
    inputs = Input((IMG_HEIGHT, IMG_WIDTH, 1))

    p1, c1 = add_unet_left_block(filters[0], add_normalization=add_normalization, 
                                 add_dropout=add_dropout, dropout_prob=dropout_prob) (inputs)
    p2, c2 = add_unet_left_block(filters[1], add_normalization=add_normalization, 
                                 add_dropout=add_dropout, dropout_prob=dropout_prob) (p1)
    p3, c3 = add_unet_left_block(filters[2], add_normalization=add_normalization, 
                                 add_dropout=add_dropout, dropout_prob=dropout_prob) (p2)
    p4, c4 = add_unet_left_block(filters[3], add_normalization=add_normalization, 
                                 add_dropout=add_dropout, dropout_prob=dropout_prob) (p3)
    p5, c5 = add_unet_left_block(filters[3], add_normalization=add_normalization, 
                                 add_dropout=add_dropout, dropout_prob=dropout_prob) (p4)

    c6 = Conv2D(filters[4], (3, 3), activation='relu', padding='same') (p5)
    c6 = Conv2D(filters[4], (3, 3), activation='relu', padding='same') (c6)

    c6 = add_unet_right_block(filters[3], add_normalization=add_normalization)(c6, c5)
    c7 = add_unet_right_block(filters[2], add_normalization=add_normalization)(c7, c4)
    c8 = add_unet_right_block(filters[1], add_normalization=add_normalization)(c8, c3)
    c9 = add_unet_right_block(filters[0], add_normalization=add_normalization)(c9, c2)
    c9 = add_unet_right_block(filters[0], add_normalization=add_normalization)(c10, c1)
    
    outputs = Conv2D(1, (1, 1), activation='sigmoid') (c10)

    model = Model(inputs=[inputs], outputs=[outputs])
    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=[dice_coef])
    return model

def train_model(model, model_path, X_train, Y_train, patience=3):
    """Trains model."""
    earlystopper = EarlyStopping(patience=patience, verbose=1)
    checkpointer = ModelCheckpoint('models/' + model_path + '-{epoch:02d}-{val_loss:.4f}' + '.h5', verbose=1, save_best_only=True, monitor='val_loss', )
    tensorboard = TensorBoard(log_dir='logs/' + model_path + '/{}'.format(time.time()))
    results = model.fit(X_train, Y_train, batch_size=10, epochs=100, validation_split=0.20,
                        callbacks=[earlystopper, checkpointer])
						
def mask2string(img):
    """Convert from mask to run-length encoding."""
    byte_array = img.reshape(img.shape[0] * img.shape[1], order='F')
    run_list = []
    count = 0
    pos = 1
    for b in byte_array:
        if b:
            count += 1
        else:
            if count:
                run_list.append((pos, count))
                pos += count
                count = 0
            pos += 1
    if count:
        run_list.append((pos, count))
        
    result = ''
    for run in run_list:
        result += f'{run[0]} {run[1]} '
        
    return result[:-1]
	
def create_submission(model_name, means, stds, IMG_HEIGHT, IMG_WIDTH, threshold=0.5, sub_filename = None):
    """Creates submission file."""
    model = load_model('models/' + model_name + '.h5', custom_objects={'dice_coef':dice_coef})
    
    get_key(*model_name.split("-")[1:3])
    height, width = model_name.split("-")[1:3]
    X_test = np.load('X_test-{}-{}.npy'.format(height, width))
    X_test_norm, _, _, _ = preprocess_data(X_test, mean = means[get_key(height, width)], std = stds[get_key(height, width)])
    
    pred = model.predict(X_test_norm)

    pixels_list = []

    pred_pic = pred
    pred_pic[pred < threshold] = 0
    pred_pic[pred >= threshold] = 1

    for i in range(len(pred)):
        mask = resize(pred_pic[i], (IMG_HEIGHT, IMG_WIDTH), mode='constant', preserve_range=True)
        mask = np.floor(mask)
        mask = mask.astype(bool)
        pixels = mask2string(mask)

        pixels_list.append(pixels)
    if sub_filename is None:
        filename = 'submissions/' + model_name + '.csv'
    else:
        filename = 'submissions/' + sub_filename + '.csv'

    with open(filename,'w') as file:
        file.write('img,pixels\n')

        for i in range(len(X_test)):
            s = str(i + 1) + ',' + pixels_list[i]
            file.write(s + '\n')

def get_key(height, width):
    return '{}-{}'.format(height, width)
